using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class car_script : MonoBehaviour
{
    [SerializeField] float steeringSpeed = 235;
    [SerializeField] float movingSpeed   = 8;
    [SerializeField] float slowSpeed = 7f;
    [SerializeField] float boostSpeed = 12f;

    void Update()
    {
        float steeringAmount    = Input.GetAxis("Horizontal") * steeringSpeed * Time.deltaTime;
        float movingDirection   = Input.GetAxis("Vertical") * movingSpeed * Time.deltaTime;

        transform.Rotate(0, 0, -steeringAmount);
        transform.Translate(0 , movingDirection, 0);
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Booster")
        {
            movingSpeed = boostSpeed;
            Destroy(other.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        movingSpeed = slowSpeed;
    }
}
