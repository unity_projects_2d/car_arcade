using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class delivery : MonoBehaviour
{
    bool hasPackage;

    SpriteRenderer spriteRenderer;
    [SerializeField] float destroyDelay = 0.5f;
    [SerializeField] Color32 hasPackageColor = new Color32 (124, 252, 0, 255);
    [SerializeField] Color32 noPackageColor = new Color32 (135 ,206 ,250, 255);

    void Start() 
    {
      spriteRenderer = GetComponent<SpriteRenderer>();
      
    }

    void OnCollisionEnter2D(Collision2D other) 
    {
      Debug.Log("Collided");      
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        // if we collide on package print "package picekd up" to the console
        if (other.tag == "Package" && !hasPackage)
        {
          Debug.Log("Package picked up");
          hasPackage = true;
          spriteRenderer.color = hasPackageColor;
          Destroy(other.gameObject, destroyDelay);
        }

        if (other.tag == "Customer" && hasPackage)
        {
          spriteRenderer.color = noPackageColor;
          Debug.Log("Delivered to Customer");
          hasPackage = false;
        }
    }
}
